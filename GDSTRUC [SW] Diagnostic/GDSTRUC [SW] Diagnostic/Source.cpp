#include <iostream>
#include <string>
#include <time.h>

using namespace std;

void linearSearch(int numArray[10], int numSearch)
{
	int steps = 0;
	bool found = false;

	for (int x = 0; x < 10; x++)
	{
		if (numArray[x] == numSearch)
		{
			cout << "The number is found \n";
			found = true;
		}

		steps++;
	}

	if (found = false)
	{
		cout << "The number is not found \n";
	}

	cout << "Steps took to search: " << steps;
}

void sortNum(int numArray[10])
{
	for (int x = 0; x < 10; x++)
	{
		int newNum = 0;

		if (numArray[x] > numArray[x + 1])
		{
			newNum = numArray[x + 1];
			numArray[x + 1] = numArray[x];
			numArray[x] = newNum;

			for (int i = 0; i < 10; i++)
			{
				if (numArray[i] > numArray[i + 1])
				{
					sortNum(numArray);
				}
			}
		}
	}
}

void assignNum(int numArray[11])
{
	srand(time(NULL));
	for (int i = 0; i < 10; i++)
	{
		numArray[i] = rand() % 69;
		cout << numArray[i] << endl;
	}
}

int main()
{
	int numArray[10] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
	int *searchNum = 0;
	
	cout << "Random values: \n";
	assignNum(numArray);
	cout << endl;

	cout << "Sorted values: \n";
	sortNum(numArray);
	for (int i = 0; i < 10; i++)
	{
		cout << numArray[i] << endl;
	}

	cout << endl;
	cout << "Please input a value to search: \n";

	cin >> *searchNum;
	linearSearch(numArray, *searchNum);
	
	system("pause");
	system("cls");
}